import 'dart:math';

import 'package:func_type_result/func_type_result.dart';

final rand = Random();

T mightFail<T>(T value) {
  if (rand.nextBool()) {
    return value;
  } else {
    throw Exception("Woopsi!");
  }
}

void main() async {
  // Create a Result
  final Result<int> ok = Ok(1);
  final Result<int> err = Err(Exception("Woopsi"));
  final res = result(() => mightFail("Some value"));
  final asyncRes = asyncResult(() async => mightFail("Some value"));

  // Do stuff with it
  final Result<String> mapped = ok.map((value) => value.toString());
  final Result<Result<int>> nested = Ok(ok);
  final Result<String> flatMapped = nested.flatMap((value) => value.toString());
  final Result<String> asyncFlatMapped =
      await nested.asyncFlatMap((value) async => value.toString());

  // TODO: Add other examples
}
