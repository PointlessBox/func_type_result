## Features

Simple functional result-type for alternative exception-handling.

## Example

```dart
final rand = Random();

T mightFail<T>(T value) {
  if (rand.nextBool()) {
    return value;
  } else {
    throw Exception("Woopsi!");
  }
}

main() {
  result(() => mightFail("Some value"))
      .map((value) => value + "success")
      .flatMap((value) => result(() => value.toInt()))
      .whenErr((ex) => println(ex));
}
```