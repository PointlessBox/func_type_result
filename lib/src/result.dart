import 'package:func_type_result/func_type_result.dart';

/// Represents a result of an operation that might fail.
sealed class Result<TOk> {}

/// Result of a successful operation. Value can be received with [Ok.value].
class Ok<TOk> implements Result<TOk> {
  final TOk value;

  Ok(this.value);
}

/// Result of a failed operation. Exception can be received with [Err.ex].
class Err<TOk> implements Result<TOk> {
  final Exception ex;

  Err(this.ex);
}

/// Calls [builder] and returns its result as [Ok] if successful.
/// If it throws and [Exception] then it returns an [Err].
/// This builder only catches [Exceptions] not [Errors].
/// An [Error] indicates an improper use of an api and should be addressed
/// by the developer.
Result<TOk> result<TOk>(
  TOk Function() builder,
) {
  try {
    return builder().ok();
  } on Exception catch (ex) {
    return ex.err();
  }
}

/// Asynchronously calls [builder] and returns its result as [Ok] if successful.
/// If it throws and [Exception] then it returns an [Err].
/// This builder only catches [Exceptions] not [Errors].
/// An [Error] indicates an improper use of an api and should be addressed
/// by the developer.
Future<Result<TOk>> asyncResult<TOk>(
  Future<TOk> Function() builder,
) async {
  try {
    return (await builder()).ok();
  } on Exception catch (ex) {
    return ex.err();
  }
}