import 'package:func_type_result/func_type_result.dart';

extension OkResultCreationExt<TOk> on TOk {
  /// Wraps [this] into [Ok].
  Result<TOk> ok() => Ok(this);
}

extension ErrResultCreationExt on Exception {
  /// Wraps [this] into [Err].
  Result<TOk> err<TOk>() => Err(this);
}
