import 'package:func_type_result/func_type_result.dart';

extension FlattenResult<TOk> on Result<Result<TOk>> {
  /// Returns the inner [Result] if present.
  Result<TOk> flatten() => this is Ok ? valueOrNull! : this as Result<TOk>;

  /// Returns the inner [Result] if present and transforms it.
  Result<TMap> flatMap<TMap>(TMap Function(TOk) transform) =>
      flatten().map(transform);

  /// Returns the inner [Result] if present and asynchronously transforms it.
  Future<Result<TMap>> asyncFlatMap<TMap>(
          Future<TMap> Function(TOk) transform) async =>
      flatten().asyncMap(transform);
}

extension FutureFlattenResult<TOk> on Future<Result<Result<TOk>>> {
  /// Returns the inner [Result] if present.
  Future<Result<TOk>> flatten() => then((value) => value.flatten());

  /// Returns the inner [Result] if present and transforms it.
  Future<Result<TMap>> flatMap<TMap>(Future<TMap> Function(TOk) transform) =>
      flatten().map(transform);
}
