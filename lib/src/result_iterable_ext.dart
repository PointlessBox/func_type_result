import '../func_type_result.dart';

extension ResultList<TOk, TErr> on Iterable<Result<TOk>> {
  /// Returns an [Iterable] containing all [Ok.value]s.
  Iterable<TOk> values() =>
      where((element) => element is Ok).map((e) => (e as Ok<TOk>).value);

  /// Returns an [Iterable] containing all [Err.ex]s.
  Iterable<Exception> errors() =>
      where((element) => element is Err).map((e) => (e as Err<TOk>).ex);
}
