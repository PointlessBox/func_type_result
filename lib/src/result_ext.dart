import '../func_type_result.dart';

extension ResultExt<TOk> on Result<TOk> {
  /// Returns [Ok.value] if present or null otherwise.
  TOk? get valueOrNull => switch (this) {
        Ok(value: final v) => v,
        Err() => null,
      };

  /// Returns [Err.ex] if present or null otherwise.
  Exception? get exOrNull => switch (this) {
        Ok() => null,
        Err(ex: final e) => e,
      };

  /// Returns [Ok.value] if present or [fallback] otherwise.
  TOk getOrDefault(
    TOk fallback,
  ) =>
      switch (this) {
        Ok(value: final v) => v,
        Err() => fallback,
      };

  /// Returns [Ok.value] if present or result of [fallback] otherwise.
  TOk getOr(
    TOk Function(Exception) fallback,
  ) =>
      switch (this) {
        Ok(value: final v) => v,
        Err(ex: final e) => fallback(e),
      };

  /// Asynchronously returns [Ok.value] if present or result of [fallback] otherwise.
  Future<TOk> asyncGetOr(
    Future<TOk> Function(Exception) fallback,
  ) async =>
      switch (this) {
        Ok(value: final v) => v,
        Err(ex: final e) => await fallback(e),
      };

  /// Transforms [Ok.value] and returns it as a new [Result].
  Result<TMap> map<TMap>(
    TMap Function(TOk) transform,
  ) =>
      switch (this) {
        Ok(value: final v) => transform(v).ok(),
        Err(ex: final e) => Err<TMap>(e),
      };

  /// Calls [block] if this is an [Ok]-instance. Returns the original instance.
  Result<TOk> whenOk(
    Function(TOk) block,
  ) {
    if (this is Ok) block((this as Ok<TOk>).value);
    return this;
  }

  /// Calls [block] if this is an [Err]-instance. Returns the original instance.
  Result<TOk> whenErr(
    Function(Exception) block,
  ) {
    if (this is Err) block((this as Err<TOk>).ex);
    return this;
  }

  /// Asynchronously transforms [Ok.value] and returns it as a new [Result].
  Future<Result<TMap>> asyncMap<TMap>(
    Future<TMap> Function(TOk) transform,
  ) async =>
      switch (this) {
        Ok(value: final v) => (await transform(v)).ok(),
        Err(ex: final e) => Err<TMap>(e),
      };

  /// Asynchronously calls [block] if this is an [Ok]-instance. Returns the original instance.
  Future<Result<TOk>> asyncWhenOk(
    Future<void> Function(TOk) block,
  ) async {
    if (this is Ok) {
      await block((this as Ok<TOk>).value);
    }
    return this;
  }

  /// Asynchronously calls [block] if this is an [Err]-instance. Returns the original instance.
  Future<Result<TOk>> asyncWhenErr(
    Future<void> Function(Exception) block,
  ) async {
    if (this is Err) {
      await block((this as Err<TOk>).ex);
    }
    return this;
  }
}

extension FutureResultExt<TOk> on Future<Result<TOk>> {
  /// Returns [Ok.value] if present or null otherwise.
  Future<TOk?> get valueOrNull => then((value) => value.valueOrNull);

  /// Returns [Err.ex] if present or null otherwise.
  Future<Exception?> get exOrNull => then((value) => value.exOrNull);

  /// Returns [Ok.value] if present or [fallback] otherwise.
  Future<TOk> getOrDefault(
    TOk fallback,
  ) =>
      then((value) => value.getOrDefault(fallback));

  /// Returns [Ok.value] if present or result of [fallback] otherwise.
  Future<TOk> getOr(
    Future<TOk> Function(Exception) fallback,
  ) =>
      then((value) async => value.asyncGetOr(fallback));

  /// Transforms [Ok.value] and returns it as a new [Result].
  Future<Result<TMap>> map<TMap>(
    Future<TMap> Function(TOk) transform,
  ) =>
      then((value) => value.asyncMap(transform));

  /// Calls [block] if this is an [Ok]-instance. Returns the original instance.
  Future<Result<TOk>> whenOk(
    Future<void> Function(TOk) block,
  ) =>
      then((value) => value.asyncWhenOk(block));

  /// Calls [block] if this is an [Err]-instance. Returns the original instance.
  Future<Result<TOk>> whenErr(
    Future<void> Function(Exception) block,
  ) =>
      then((value) => value.asyncWhenErr(block));
}
