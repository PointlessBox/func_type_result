/// Functional [Result]-type with extension-methods such as .map, .flatMap etc.
/// for alternative error-handling.
library;

export 'src/result.dart';
export 'src/result_creation_ext.dart';
export 'src/result_ext.dart';
export 'src/result_flatten_ext.dart';
export 'src/result_iterable_ext.dart';