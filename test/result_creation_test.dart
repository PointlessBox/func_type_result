import 'package:func_type_result/func_type_result.dart';
import 'package:test/test.dart';

void main() {
  group('Creation', () {

    setUp(() {
      // Additional setup goes here.
    });

    test('Create Ok result', () {
      final Result<int> ok = Ok(1);
      assert(ok is Ok);
    });

    test('Create Err result', () {
      final Result<int> err = Err(Exception(""));
      assert(err is Err);
    });

    test('result-builder', () {
      expect(result(() => 1).valueOrNull, 1);
      final ex = Exception("");
      expect(result(() => throw ex).exOrNull, ex);
      expect(result(() => throw ex).exOrNull, ex);
      try {
        // Result should not catch an error, because errors are not meant to be caught.
        result(() => throw Error());
      } on Error catch (_) {
        assert(true);
      }
    });

    test('async result-builder', () async {
      expect((await asyncResult(() async => 1)).valueOrNull, 1);
      final ex = Exception("");
      expect((await asyncResult(() => throw ex)).exOrNull, ex);
      try {
        // Result should not catch an error, because errors are not meant to be caught.
        await asyncResult(() => throw Error());
      } on Error catch (_) {
        assert(true);
      }
    });

    test('Create Ok with Object.ok()', () {
      final Result<int> ok = 1.ok();
      assert(ok is Ok);
    });

    test('Create Err with Object.err()', () {
      final Result<int> err = Exception("").err();
      assert(err is Err);
    });
  });
}
