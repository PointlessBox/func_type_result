import 'package:func_type_result/src/result_creation_ext.dart';
import 'package:func_type_result/src/result_iterable_ext.dart';
import 'package:test/test.dart';

void main() {
  group('Iterable', () {

    setUp(() {
      // Additional setup goes here.
    });

    test('values', () {
      final results = [
        1.ok(),
        2.ok(),
        Exception("1").err(),
        Exception("2").err(),
      ];
      expect(results.values(), [1, 2]);
    });

    test('errors', () {
      final ex1 = Exception("1");
      final ex2 = Exception("2");
      final results = [
        1.ok(),
        2.ok(),
        ex1.err(),
        ex2.err(),
      ];
      expect(results.errors(), [ex1, ex2]);
    });
  });
}
