import 'package:func_type_result/func_type_result.dart';
import 'package:test/test.dart';

void main() {
  group('Extensions', () {
    setUp(() {
      // Additional setup goes here.
    });

    test('valueOrNull', () {
      expect(Ok(1).valueOrNull, 1);
      expect(Err(Exception("")).valueOrNull, null);
    });

    test('exOrNull', () {
      expect(Ok(1).exOrNull, null);
      final ex = Exception("");
      expect(Err(ex).exOrNull, ex);
    });

    test('getOrElse', () {
      expect(Ok(1).getOrDefault(2), 1);
      expect(Err(Exception("")).getOrDefault(2), 2);
    });

    test('getOr', () {
      expect(Ok(1).getOr((_) => 2), 1);
      expect(Err(Exception("")).getOr((_) => 2), 2);
    });

    test('asyncGetOr', () async {
      expect(await Ok(1).asyncGetOr((_) async => 2), 1);
      expect(await Err(Exception("")).asyncGetOr((_) async => 2), 2);
    });

    test('map', () {
      expect(Ok(1)
          .map((v) => v.toString())
          .valueOrNull, "1");
      expect(Err(Exception(""))
          .map((v) => v.toString())
          .valueOrNull, null);
    });

    test('whenOk', () {
      bool wasCalled = false;
      block(_) => wasCalled = true;
      Ok(1).whenOk(block);
      expect(wasCalled, isTrue);

      wasCalled = false;
      Err(Exception("")).whenOk(block);
      expect(wasCalled, isFalse);
    });

    test('whenErr', () {
      bool wasCalled = false;
      block(_) => wasCalled = true;
      Ok(1).whenErr(block);
      expect(wasCalled, isFalse);

      wasCalled = false;
      Err(Exception("")).whenErr(block);
      expect(wasCalled, isTrue);
    });

    test('asyncMap', () async {
      expect(
          (await Ok(1).asyncMap((v) async => v.toString())).valueOrNull, "1");
      expect(
          (await Err(Exception("")).asyncMap((v) async => v.toString()))
              .valueOrNull,
          null);
    });

    test('asyncWhenOk', () async {
      bool wasCalled = false;
      block(_) async => wasCalled = true;
      await Ok(1).asyncWhenOk(block);
      expect(wasCalled, isTrue);

      wasCalled = false;
      await Err(Exception("")).asyncWhenOk(block);
      expect(wasCalled, isFalse);
    });

    test('asyncWhenErr', () async {
      bool wasCalled = false;
      block(_) async => wasCalled = true;
      await Ok(1).asyncWhenErr(block);
      expect(wasCalled, isFalse);

      wasCalled = false;
      await Err(Exception("")).asyncWhenErr(block);
      expect(wasCalled, isTrue);
    });
  });

  group('Future extensions', () {
    setUp(() {
      // Additional setup goes here.
    });

    test('valueOrNull', () async {
      expect(await Future(() => Ok(1)).valueOrNull, 1);
      expect(await Future(() => Err(Exception(""))).valueOrNull, null);
    });

    test('exOrNull', () async {
      expect(await Future(() => Ok(1)).exOrNull, null);
      final ex = Exception("");
      expect(await Future(() => Err(ex)).exOrNull, ex);
    });

    test('getOrElse', () async {
      expect(await Future(() => Ok(1)).getOrDefault(2), 1);
      expect(await Future(() => Err(Exception(""))).getOrDefault(2), 2);
    });

    test('getOr', () async {
      expect(await Future(() => Ok(1)).getOr((_) async => 2), 1);
      expect(await Future(() => Err(Exception(""))).getOr((_) async => 2), 2);
    });

    test('asyncGetOr', () async {
      expect(await Ok(1).asyncGetOr((_) async => 2), 1);
      expect(await Err(Exception("")).asyncGetOr((_) async => 2), 2);
    });

    test('map', () async {
      expect(await Future(() => Ok(1))
          .map((v) async => v.toString())
          .valueOrNull, "1");
      expect(await Future(() => Err(Exception("")))
          .map((v) async => v.toString())
          .valueOrNull, null);
    });

    test('whenOk', () async {
      bool wasCalled = false;
      block(_) async => wasCalled = true;
      await Future(() => Ok(1)).whenOk(block);
      expect(wasCalled, isTrue);

      wasCalled = false;
      await Future(() => Err(Exception(""))).whenOk(block);
      expect(wasCalled, isFalse);
    });

    test('whenErr', () async {
      bool wasCalled = false;
      block(_) async => wasCalled = true;
      await Future(() => Ok(1)).whenErr(block);
      expect(wasCalled, isFalse);

      wasCalled = false;
      await Future(() => Err(Exception(""))).whenErr(block);
      expect(wasCalled, isTrue);
    });
  });
}
