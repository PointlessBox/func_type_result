import 'package:func_type_result/func_type_result.dart';
import 'package:test/test.dart';

void main() {
  group('Flatten', () {
    setUp(() {
      // Additional setup goes here.
    });

    test('flatten', () {
      expect(Ok(Ok(1)).flatten().valueOrNull, 1);
    });

    test('flatMap', () {
      expect(Ok(Ok(1)).flatMap((v) => v.toString()).valueOrNull, "1");
    });

    test('asyncFlatMap', () async {
      expect(
          (await Ok(Ok(1)).asyncFlatMap((v) async => v.toString())).valueOrNull,
          "1");
    });
  });

  group('Future flatten', () {
    setUp(() {
      // Additional setup goes here.
    });

    test('flatten', () async {
      expect(await Future(() => Ok(Ok(1))).flatten().valueOrNull, 1);
    });

    test('flatMap', () async {
      expect(
          await Future(() => Ok(Ok(1)))
              .flatMap((v) async => v.toString())
              .valueOrNull,
          "1");
    });
  });
}
